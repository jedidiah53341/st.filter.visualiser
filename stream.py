import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime

st.header('Simple .csv filter & visualiser')
st.caption('Upload a .csv file on the sidebar, then pick which column do you want to group by sum.')

def makechart() :
    grouped_data = dataframe.groupby(y_axis)[x_axis].sum().reset_index()
    df2 = grouped_data.set_index(y_axis)
    chart_data = pd.DataFrame(df2,columns=[x_axis])
    if chart_type == 'Line chart' :
        st.line_chart(chart_data)
    if chart_type == 'Area chart' :
        st.area_chart(chart_data)
    if chart_type == 'Bar chart' :
        st.bar_chart(chart_data)
        
def temporary_date():
    dataframe[column_name] = dataframe[base_col].apply(lambda x: datetime.datetime.strptime(x, format_from).strftime(format_to))
    st.caption("Here's the updated dataframe. If you wish to use it for further visualisation, please download this dataframe and reload it back using the upload prompt at the top of the sidebar.")
    st.write(dataframe.head())

    @st.cache
    def convert_df(df):
        # IMPORTANT: Cache the conversion to prevent computation on every rerun
        return df.to_csv().encode('utf-8')
    csv = convert_df(dataframe)
    st.download_button(label="Download updated .csv",data=csv,file_name='updated_csv.csv',mime='text/csv',)


    
uploaded_file = st.sidebar.file_uploader("Choose a file")
if uploaded_file is not None:
     # Can be used wherever a "file-like" object is accepted:
     dataframe = pd.read_csv(uploaded_file)
     st.subheader("Data header")
     st.write(dataframe.head())
     st.subheader("Descriptive statistics")
     st.write(dataframe.describe())
     st.sidebar.subheader("Columns with null values")
     st.sidebar.write(dataframe.isna().sum())
     

     st.sidebar.header("Visualise grouped sum")
     x_axis = st.sidebar.selectbox('Which column do you want to group by sum?',list(dataframe.columns),index=1)
     y_axis = st.sidebar.selectbox('Which column do you want group by?',list(dataframe.columns),index=2)
     chart_type = st.sidebar.selectbox('What kind of chart do you want to use?',['Line chart','Area chart','Bar chart'])
     if st.sidebar.button('Generate Chart'):
        makechart()

     st.sidebar.header("Get a filtered dataframe")
     number_of_filter = st.sidebar.radio("How much condition do you want to apply the filter with?",('1', '2', '3'))

     if number_of_filter == '1':
         st.sidebar.subheader("First filter")
         filter_column = st.sidebar.selectbox('Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand = st.sidebar.selectbox('must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value = st.sidebar.text_input('to..', '')

     if number_of_filter == '2':
         st.sidebar.subheader("First filter")
         filter_column = st.sidebar.selectbox('Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand = st.sidebar.selectbox('must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value = st.sidebar.text_input('to..', '')
         st.sidebar.subheader("Second filter")
         filter_column2 = st.sidebar.selectbox('(2) Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand2 = st.sidebar.selectbox('(2) must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value2 = st.sidebar.text_input('(2) to..', '')
         
     if number_of_filter == '3':
         st.sidebar.subheader("First filter")
         filter_column = st.sidebar.selectbox('Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand = st.sidebar.selectbox('must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value = st.sidebar.text_input('to..', '')
         st.sidebar.subheader("Second filter")
         filter_column2 = st.sidebar.selectbox('(2) Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand2 = st.sidebar.selectbox('(2) must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value2 = st.sidebar.text_input('(2) to..', '')
         st.sidebar.subheader("Third filter")
         filter_column3 = st.sidebar.selectbox('(3) Display a dataframe where the values on column..',list(dataframe.columns))
         filter_operand3 = st.sidebar.selectbox('(3) must be..',['less than','equal to','greater than','equal to (strings)','not equal to (strings)'])
         filter_value3 = st.sidebar.text_input('(3) to..', '')

     def filter_dataframe():
         if number_of_filter == '1':
             if filter_operand == 'less than':
                 filtered = dataframe.loc[dataframe[filter_column] < float(filter_value)]
             elif filter_operand == 'equal to':
                 filtered = dataframe.loc[dataframe[filter_column] == float(filter_value)]
             elif filter_operand == 'greater than':
                 filtered = dataframe.loc[dataframe[filter_column] > float(filter_value)]
             elif filter_operand == 'equal to (strings)':
                 filtered = dataframe.loc[dataframe[filter_column] == filter_value]
             elif filter_operand == 'not equal to (strings)':
                 filtered = dataframe.loc[dataframe[filter_column] != filter_value]
                 
             def convert_df(df):
                 # IMPORTANT: Cache the conversion to prevent computation on every rerun
                 return df.to_csv().encode('utf-8')
             csv = convert_df(filtered)
             st.caption("Here's the filtered dataframe. If you wish to use it for further visualisation, please download this dataframe and reload it back using the upload prompt at the top of the sidebar.")
             st.download_button(label="Download filtered .csv",data=csv,file_name='filtered_csv.csv',mime='text/csv',)
             st.write(filtered)
             
         if number_of_filter == '2':
             if filter_operand == 'less than':
                 filtered = dataframe.loc[dataframe[filter_column] < float(filter_value)]
             elif filter_operand == 'equal to':
                 filtered = dataframe.loc[dataframe[filter_column] == float(filter_value)]
             elif filter_operand == 'greater than':
                 filtered = dataframe.loc[dataframe[filter_column] > float(filter_value)]
             elif filter_operand == 'equal to (strings)':
                 filtered = dataframe.loc[dataframe[filter_column] == filter_value]
             elif filter_operand == 'not equal to (strings)':
                 filtered = dataframe.loc[dataframe[filter_column] != filter_value]
                 
             if filter_operand2 == 'less than':
                 filtered2 = filtered.loc[filtered[filter_column2] < float(filter_value2)]
             elif filter_operand2 == 'equal to':
                 filtered2 = filtered.loc[filtered[filter_column2] == float(filter_value2)]
             elif filter_operand2 == 'greater than':
                 filtered2 = filtered.loc[filtered[filter_column2] > float(filter_value2)]
             elif filter_operand == 'equal to (strings)':
                 filtered2 = filtered.loc[filtered[filter_column2] == filter_value2]
             elif filter_operand == 'not equal to (strings)':
                 filtered2 = filtered.loc[filtered[filter_column2] != filter_value2]
                 
             def convert_df(df):
                 # IMPORTANT: Cache the conversion to prevent computation on every rerun
                 return df.to_csv().encode('utf-8')
             csv = convert_df(filtered2)
             st.caption("Here's the filtered dataframe. If you wish to use it for further visualisation, please download this dataframe and reload it back using the upload prompt at the top of the sidebar.")
             st.download_button(label="Download filtered .csv",data=csv,file_name='filtered_csv.csv',mime='text/csv',)
             st.write(filtered2)

         if number_of_filter == '3':
             if filter_operand == 'less than':
                 filtered = dataframe.loc[dataframe[filter_column] < float(filter_value)]
             elif filter_operand == 'equal to':
                 filtered = dataframe.loc[dataframe[filter_column] == float(filter_value)]
             elif filter_operand == 'greater than':
                 filtered = dataframe.loc[dataframe[filter_column] > float(filter_value)]
                 
             if filter_operand2 == 'less than':
                 filtered2 = filtered.loc[filtered[filter_column2] < float(filter_value2)]
             elif filter_operand2 == 'equal to':
                 filtered2 = filtered.loc[filtered[filter_column2] == float(filter_value2)]
             elif filter_operand2 == 'greater than':
                 filtered2 = filtered.loc[filtered[filter_column2] > float(filter_value2)]
             elif filter_operand == 'equal to (strings)':
                 filtered2 = filtered.loc[filtered[filter_column2] == filter_value2]
             elif filter_operand == 'not equal to (strings)':
                 filtered2 = filtered.loc[filtered[filter_column2] != filter_value2]

             if filter_operand3 == 'less than':
                 filtered3 = filtered2.loc[filtered2[filter_column3] < float(filter_value3)]
             elif filter_operand2 == 'equal to':
                 filtered3 = filtered2.loc[filtered2[filter_column3] == float(filter_value3)]
             elif filter_operand2 == 'greater than':
                 filtered3 = filtered2.loc[filtered2[filter_column3] > float(filter_value3)]
             elif filter_operand == 'equal to (strings)':
                 filtered3 = filtered2.loc[filtered2[filter_column3] == filter_value3]
             elif filter_operand == 'not equal to (strings)':
                 filtered3 = filtered2.loc[filtered2[filter_column3] != filter_value3]
                 
             def convert_df(df):
                 # IMPORTANT: Cache the conversion to prevent computation on every rerun
                 return df.to_csv().encode('utf-8')
             csv = convert_df(filtered3)
             st.caption("Here's the filtered dataframe. If you wish to use it for further visualisation, please download this dataframe and reload it back using the upload prompt at the top of the sidebar.")
             st.download_button(label="Download filtered .csv",data=csv,file_name='filtered_csv.csv',mime='text/csv',)
             st.write(filtered3)
         
     if st.sidebar.button('Generate filtered dataframe'):
         filter_dataframe()

     st.sidebar.header("Create additional datetime column")
     base_col = st.sidebar.selectbox('Which column do you want to change the date format to?',list(dataframe.columns),index=3)
     format_from = st.sidebar.text_input('Which column do you want to change the date format to?','%Y-%m-%d')
     format_to = st.sidebar.text_input('Which column do you want to change the date format to?','%Y-%m')
     column_name = st.sidebar.text_input('Enter the name of the new date column.', 'order_month')
     if st.sidebar.button('Generate temporary date column'):
        temporary_date()



