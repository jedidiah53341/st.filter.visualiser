# st.filter.visualiser

This is a streamlit application meant to provide an easy way to visualize .csv data. Using this app, users can apply a maximum of 3 operators-based filters and generate additional datetime columns based on pre-existing datetime information. Though, the only supported visualization is in the form of grouped sum. ( "sum the values of x for each unique values of y " ). It is deployed on Heroku, here : https://st-filter-visualise.herokuapp.com/

It's just a small app that i made while learning the uses of streamlit. I think i would make another one of this that simulates the steps of machine learning once i'm more familiar with it. 
